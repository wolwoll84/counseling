package com.ksy.counseling.service;

import com.ksy.counseling.entity.Customer;
import com.ksy.counseling.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(String name, String phone) {
        Customer addData = new Customer();
        addData.setName(name);
        addData.setPhone(phone);

        customerRepository.save(addData);
    }
}
